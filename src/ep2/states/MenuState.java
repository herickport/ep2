package ep2.states;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import ep2.Game;
import ep2.display.Display;

public class MenuState extends State {
	
	/**
	 * Construtor da classe MenuState
	 * 
	 * @param game
	 */
	public MenuState(Game game) {
		super(game);
	} 
	
	/**
	 * Update para iniciar o gameState de acordo com a entrada
	 * do teclado dada pelo usuário
	 */
	@Override
	public void update() {
		if(getGame().getKeyManager().enter || getGame().getKeyManager().map0 ||
				getGame().getKeyManager().map1 || getGame().getKeyManager().map2 ||
				getGame().getKeyManager().map3 || getGame().getKeyManager().map4 ||
				getGame().getKeyManager().map5) {
			State.setCurrentState(new GameState(getGame()));
		}
	}
	
	/**
	 * Desenha o menu na tela... O menu está muito feio kkk
	 */
	@Override
	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, Display.WIDTH, Display.HEIGHT);
		g.setColor(Color.WHITE);
		g.setFont(new Font("TimesRoman", Font.BOLD, 75));
		g.drawString("Snake Game", 270, 150);
		g.setFont(new Font("arial", Font.CENTER_BASELINE, 15));
		g.drawString("Pressione [0,1,2,3,4,5] para escolher o mapa", 330, 315);
		g.drawString("OU", Display.WIDTH / 2, Display.HEIGHT / 2 + 50);
		g.drawString("[Enter] para iniciar o jogo em um mapa aleatório", 320, 360);
	}

}
