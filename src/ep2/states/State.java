package ep2.states;

import java.awt.Graphics;

import ep2.Game;

public abstract class State {
	
	// State em execução
	private static State currentState = null;

	/**
	 * @return currentState
	 */
	public static State getCurrentState() {
		return currentState;
	}
	
	/**
	 * @param currentState
	 */
	public static void setCurrentState(State currentState) {
		State.currentState = currentState;
	}
	
	protected Game game;
	
	public State(Game game) {
		this.game = game;
	}

	public Game getGame() {
		return game;
	}

	public abstract void update();
	
	public abstract void draw(Graphics g);
}
