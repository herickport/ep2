package ep2.states;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import ep2.Game;
import ep2.display.Display;
import ep2.entities.fruits.BigFruit;
import ep2.entities.fruits.BombFruit;
import ep2.entities.fruits.DecreaseFruit;
import ep2.entities.fruits.Fruit;
import ep2.entities.fruits.SimpleFruit;
import ep2.entities.snakes.CommonSnake;
import ep2.entities.snakes.Snake;
import ep2.maps.Map;

public class GameState extends State {

	public static boolean gameOver = false;
	
	private Snake snake;
	private Map map;
	private Fruit simpleFruit, bigFruit, decreaseFruit, bombFruit;
	private Random mapNumber;
	private int totalPoints;
	private int numberOfFruits;
	
	/**
	 * Construtor da classe GameState
	 * 
	 * @param game
	 */
	public GameState(Game game) {
		super(game);
				
		// Carrega o mapa
		mapNumber = new Random(); 
		loadMap();
		
		// cria a snake inicial do jogo (Comum) e seta ela como a snake em uso
		this.snake = new CommonSnake(game, map, Display.WIDTH / 2, Display.HEIGHT / 2);
		Snake.setCurrentSnake(snake);
		
		// Cria todos os tipos de frutas e inicializa a thread da simple fruit
		this.simpleFruit = new SimpleFruit(game, map);
		this.bigFruit = new BigFruit(game, map);
		this.decreaseFruit = new DecreaseFruit(game, map);
		this.bombFruit = new BombFruit(game, map);
		new Thread(simpleFruit).start();
		
		this.numberOfFruits = 1;
		this.totalPoints = 0;
	}

	/**
	 * Faz o update da snake, frutas, calcula os pontos, e
	 * chama a função para verificar se vai setar uma nova
	 * fruta ou trocar de snake durante a partida
	 */
	@Override
	public void update() {
		Snake.getCurrentSnake().update();
		calcPoints();
		fruitUpdate();
		
		if(isGameOver()) {
			State.setCurrentState(new GameOverState(game, this.totalPoints));
			GameState.setGameOver(false);
		}
		
		setSpecialFruitOrNewSnake();
	}
	
	/**
	 * Renderiza o mapa, cobra, frutas e informações do
	 * jogo (tamanho da snake, velocidade, pontos) na tela
	 */
	@Override
	public void draw(Graphics g) {
		map.draw(g);
		Snake.getCurrentSnake().draw(g);
		fruitDraw(g);
		
		g.setColor(Color.BLUE);
		g.drawString("Tamanho da Snake: " + Snake.getCurrentSnake().getSnakeSize(), Display.WIDTH - 1000, Display.HEIGHT - 20);
		g.drawString("Pontuação: " + this.totalPoints, Display.WIDTH - 110, Display.HEIGHT - 20);
	}
	
	/**
	 * Calcula os pontos baseado na pontuação dada pela fruta e pela
	 * snake usada no momento (pontuação dobrada para a StarSnake)
	 */
	public void calcPoints() {
		if(simpleFruit.eatFruit()) {
			this.totalPoints += simpleFruit.getPoints() * Snake.getCurrentSnake().getPointsRate();
		} else if(bigFruit.eatFruit()) {
			this.totalPoints += bigFruit.getPoints() * Snake.getCurrentSnake().getPointsRate();
		}
	}
	
	/**
	 * Faz o update de todas as frutas
	 */
	public void fruitUpdate() {
		simpleFruit.update();
		bombFruit.update();
		bigFruit.update();
		decreaseFruit.update();
	}
	
	/**
	 * Desenha as frutas na tela
	 */
	public void fruitDraw(Graphics g) {
		simpleFruit.draw(g);
		bombFruit.draw(g);
		bigFruit.draw(g);
		decreaseFruit.draw(g);
	}
	
	/**
	 * Verifica a pontuação para ativar a Thread de setar um fruta especial na tela
	 * garantindo que fique no máximo 2 frutas ao mesmo tempo, e para ativar a
	 * Thread que troca a Snake por um determinado período 
	 */
	public void setSpecialFruitOrNewSnake() {
		if(this.totalPoints % 30 == 0 && this.totalPoints > 0 && numberOfFruits == 1) {
			if(this.totalPoints % 60 == 0) { 
				new Thread(Snake.getCurrentSnake()).start();			
			}
			Random random = new Random();
			int randomNumber = random.nextInt(4);
			// 50% de chance de ser bigFruit
			if(randomNumber == 0 || randomNumber == 1) {
				new Thread(bigFruit).start();
			// 25% de chance de ser decreaseFruit
			} else if(randomNumber == 2) {
				new Thread(decreaseFruit).start();
			// 25% de chance de ser bombFruit
			} else if(randomNumber == 3) {
				new Thread(bombFruit).start();
			}
			numberOfFruits++;
		} else if(this.totalPoints % 30 != 0) {
			numberOfFruits = 1;
		}
	}
	
	/**
	 * Carrega o mapa de acordo com a escolha do usuário[0..5]
	 * ou aleatoriamente [enter]
	 */
	public void loadMap() {
		if(getGame().getKeyManager().map0) {
			this.map = new Map(getGame(), "./res/maps/map0.txt");
		} else if(getGame().getKeyManager().map1) {
			this.map = new Map(getGame(), "./res/maps/map1.txt");
		} else if(getGame().getKeyManager().map2) {
			this.map = new Map(getGame(), "./res/maps/map2.txt");
		} else if(getGame().getKeyManager().map3) {
			this.map = new Map(getGame(), "./res/maps/map3.txt");
		} else if(getGame().getKeyManager().map4) {
			this.map = new Map(getGame(), "./res/maps/map4.txt");
		} else if(getGame().getKeyManager().map5) {
			this.map = new Map(getGame(), "./res/maps/map5.txt");
		} else {
			this.map = new Map(getGame(), "./res/maps/map" + mapNumber.nextInt(6) + ".txt");
		}
	}

	public static boolean isGameOver() {
		return gameOver;
	}

	public static void setGameOver(boolean gameOver) {
		GameState.gameOver = gameOver;
	}
	
}
