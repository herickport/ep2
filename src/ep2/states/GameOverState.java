package ep2.states;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import ep2.Game;
import ep2.display.Display;
import ep2.utils.Utils;

public class GameOverState extends State {
	
	private int points;
	
	public GameOverState(Game game, int points) {
		super(game);
		this.points = points;
		
		// Salva os pontos obtidos em um arquivo
		Utils.loadFileAndWritePoints("./res/ranking/score.txt", this.points);
	}

	/**
	 * Verifica se o usuário clicou no espaço
	 * para retornar ao menuState
	 */
	@Override
	public void update() {
		if(getGame().getKeyManager().space) {
			State.setCurrentState(new MenuState(getGame()));
		}
	}
	
	/**
	 * Desenha a tela de Game Over
	 */
	@Override
	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, Display.WIDTH, Display.HEIGHT);
		g.setColor(Color.WHITE);
		g.setFont(new Font("arial", Font.BOLD, 75));
		g.drawString("Game Over!", 250, 200);
		g.setFont(new Font("arial", Font.ITALIC, 20));
		g.drawString("Você conquistou " + this.points + " pontos", 360, 310);
		g.drawString("Pressione espaço para retornar ao menu", 300, 340);
	}

}
