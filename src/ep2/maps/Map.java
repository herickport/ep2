package ep2.maps;

import java.awt.Graphics;

import ep2.Game;
import ep2.blocks.BackgroundBlock;
import ep2.blocks.Block;
import ep2.blocks.SolidBlock;
import ep2.display.Display;
import ep2.utils.Utils;

public class Map {
	
	@SuppressWarnings("unused")
	private Game game;
	private Block block;
	private int[][] tiles;
	
	/**
	 * Construtor da classe Map
	 * 
	 * @param game
	 * @param path
	 */
	public Map(Game game, String path) {
		this.game = game;
		loadMap(path);
	}
	
	/**
	 * Desenha o mapa carregado na tela
	 * 
	 * @param g
	 */
	public void draw(Graphics g) {
		for(int y = 0; y < Display.HEIGHT / Block.DEFAULT_BLOCK_DIMENSION - 1; y++) {
			for(int x = 0; x < Display.WIDTH / Block.DEFAULT_BLOCK_DIMENSION; x++) {
				getBlock(x, y).draw(g, x * Block.DEFAULT_BLOCK_DIMENSION, y * Block.DEFAULT_BLOCK_DIMENSION);
			}
		}
	}
	
	/**
	 * Verifica o tipo de bloco do pixel x, y
	 * 
	 * @param x
	 * @param y
	 * 
	 * @return block
	 * 			O bloco existente no pixel x, y
	 */
	public Block getBlock(int x, int y) {
		if(tiles[x][y] == 0) {
			block = new BackgroundBlock();
			return block;
		} else {
			block = new SolidBlock();
			return block;
		}
	}
	
	/**
	 * Carrega o mapa a partir de um arquivo .txt e salva em uma matriz
	 * para ser desenhado na tela posteriormente
	 * 
	 * @param path
	 */
	public void loadMap(String path) {
		String file = Utils.loadFileAsString(path);
		String[] tokens = file.split("\\s+");
		
		tiles = new int[Display.WIDTH / Block.DEFAULT_BLOCK_DIMENSION][Display.HEIGHT / Block.DEFAULT_BLOCK_DIMENSION - 1];
		int i = 0;
		
		for(int y = 0; y < Display.HEIGHT / Block.DEFAULT_BLOCK_DIMENSION - 1; y++) {
			for(int x = 0; x < Display.WIDTH / Block.DEFAULT_BLOCK_DIMENSION; x++) {
				tiles[x][y] = Utils.parseInt(tokens[i++]);
			}
		}
	}
	
	

}
