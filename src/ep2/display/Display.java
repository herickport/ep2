package ep2.display;

import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;

public class Display {
	
	// Atributos estáticos
	public static final String TITLE = "Snake";
	public static final int WIDTH = 1008;
	public static final int HEIGHT = 576;
	public static final Dimension SIZE = new Dimension(WIDTH, HEIGHT);
	
	private JFrame frame;
	private Canvas canvas;
	
	/**
	 * Construtor do Display do game,
	 * setando as características do Jframe e do Canvas
	 */
	public Display() {
		frame = new JFrame(TITLE);
		
		frame.setSize(WIDTH, HEIGHT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		canvas = new Canvas();
		canvas.setPreferredSize(SIZE);
		canvas.setMaximumSize(SIZE);
		canvas.setMinimumSize(SIZE);
		canvas.setFocusable(false);
		
		frame.add(canvas);
		frame.pack();
	}
	
	// GETTERS E SETTERS
	
	public JFrame getFrame() {
		return frame;
	}

	public Canvas getCanvas() {
		return canvas;
	}

}
