package ep2.entities.snakes;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import ep2.Game;
import ep2.maps.Map;

public class CommonSnake extends Snake {

	public CommonSnake(Game game, Map map, int xPosition, int yPosition) {
		super(game, map, xPosition, yPosition);
	}
	
	public CommonSnake(Game game, Map map, Snake oldSnake) {
		super(game, map, oldSnake);
	}
	
	/**
	 * Desenha a CommonSnake na tela do game
	 */
	@Override
	public void draw(Graphics g) {
		ImageIcon image = new ImageIcon("./res/snake.png");
		snakeImage = image.getImage();
		for(int i = 0; i < snakeXPosition.size(); i++) {
			g.drawImage(snakeImage, snakeXPosition.get(i), snakeYPosition.get(i), null);
		}
	}

}
