package ep2.entities.snakes;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import ep2.Game;
import ep2.blocks.Block;
import ep2.display.Display;
import ep2.maps.Map;
import ep2.states.GameState;

public class KittySnake extends Snake {

	public KittySnake(Game game, Map map, int xPosition, int yPosition) {
		super(game, map, xPosition, yPosition);
	}
	
	public KittySnake(Game game, Map map, Snake oldSnake) {
		super(game, map, oldSnake);
	}
	
	/**
	 * Desenha a KittySnake na tela do game
	 */
	@Override
	public void draw(Graphics g) {
		ImageIcon image = new ImageIcon("./res/kittySnake.gif");
		snakeImage = image.getImage();
		for(int i = 0; i < snakeXPosition.size(); i++) {
			g.drawImage(snakeImage, snakeXPosition.get(i), snakeYPosition.get(i), null);
		}
	}
	
	/**
	 * Override da movimentação da KittySnake...
	 * 
	 * Apesar de atravessar os blocos sólidos a kittySnake
	 * não pode atravessar as bordas da tela
	 */
	@Override
	public void moveX() {
		if((int) xMove == DEFAULT_ENTITY_DIMENSION || (int) xMove == -DEFAULT_ENTITY_DIMENSION) {
			xPosition += (int) xMove;
			if(xPosition < 0) {
				GameState.setGameOver(true);
				//State.setCurrentState(new GameOverState(game));
			} else if(xPosition > Display.WIDTH - DEFAULT_ENTITY_DIMENSION) {
				GameState.setGameOver(true);
				//State.setCurrentState(new GameOverState(game));
			}
			
			snakeXPosition.add(0, xPosition);
			snakeYPosition.add(0, yPosition);
			snakeXPosition.remove(snakeSize);
			snakeYPosition.remove(snakeSize);
			
			this.snakeXHead = snakeXPosition.get(0);
			this.snakeYHead = snakeYPosition.get(0);
			
			xMove = 0;
		}
	}
	
	/**
	 * Override da movimentação da KittySnake...
	 * 
	 * Apesar de atravessar os blocos sólidos a kittySnake
	 * não pode atravessar as bordas da tela
	 */
	@Override
	public void moveY() {
		if((int) yMove == DEFAULT_ENTITY_DIMENSION || (int) yMove == -DEFAULT_ENTITY_DIMENSION) {
			yPosition += (int) yMove;
			if(yPosition < 0) {
				GameState.setGameOver(true);
				//State.setCurrentState(new GameOverState(game));
			} else if(yPosition > Display.HEIGHT - DEFAULT_ENTITY_DIMENSION - Block.DEFAULT_BLOCK_DIMENSION) {
				GameState.setGameOver(true);
				//State.setCurrentState(new GameOverState(game));
			}
			
			snakeXPosition.add(0, xPosition);
			snakeYPosition.add(0, yPosition);
			snakeXPosition.remove(snakeSize);
			snakeYPosition.remove(snakeSize);
			
			this.snakeXHead = snakeXPosition.get(0);
			this.snakeYHead = snakeYPosition.get(0);
			
			yMove = 0;
			
		}
	}
	
	/**
	 * A Kitty Snake não colide com os blocos,
	 * retorno sempre falso
	 */
	@Override
	public boolean collisionWithBlock() {
		return false;
	}

}
