package ep2.entities.snakes;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import ep2.Game;
import ep2.maps.Map;

public class StarSnake extends Snake {

	public StarSnake(Game game, Map map, int xPosition, int yPosition) {
		super(game, map, xPosition, yPosition);
		// A StarSnake possui pontuação em dobro
		this.pointsRate = 2;
	}

	public StarSnake(Game game, Map map, Snake oldSnake) {
		super(game, map, oldSnake);
		// A StarSnake possui pontuação em dobro
		this.pointsRate = 2;
	}

	/**
	 * Desenha a StarSnake na tela do game
	 */
	@Override
	public void draw(Graphics g) {
		ImageIcon image = new ImageIcon("./res/starSnake.gif");
		snakeImage = image.getImage();
		for(int i = 0; i < snakeXPosition.size(); i++) {
			g.drawImage(snakeImage, snakeXPosition.get(i), snakeYPosition.get(i), null);
		}
	}

}
