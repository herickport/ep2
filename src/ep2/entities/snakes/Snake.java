package ep2.entities.snakes;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Random;

import ep2.Game;
import ep2.blocks.Block;
import ep2.display.Display;
import ep2.entities.Entity;
import ep2.maps.Map;
import ep2.states.GameState;

/**
 * Classe abstrata Snake que será a base para todas as classes de snake
 * 
 * @author herickport
 *
 */
public abstract class Snake extends Entity implements Runnable {
	
	// Snake em uso
	public static Snake currentSnake = null;
	public static final float DEFAULT_SPEED = 0.6f;
	
	private Thread thread;
	
	protected ArrayList<Integer> snakeXPosition;
	protected ArrayList<Integer> snakeYPosition;
	protected Image snakeImage;
	protected int snakeSize;
	
	protected int snakeXHead;
	protected int snakeYHead;
	
	protected float speed;
	protected float xMove;
	protected float yMove;
	protected int pointsRate;
	
	/**
	 * Construtor padrão da Snake
	 * 
	 * @param game
	 * @param map
	 * @param xPosition
	 * @param yPosition
	 */
	public Snake(Game game, Map map, int xPosition, int yPosition) {
		super(game, map, xPosition, yPosition);
		
		snakeXPosition = new ArrayList<Integer>();
		snakeYPosition = new ArrayList<Integer>();
		
		// Snake inicial com tamanho 3
		this.snakeXPosition.add(xPosition);
		this.snakeYPosition.add(yPosition);
		this.snakeXPosition.add(xPosition - DEFAULT_ENTITY_DIMENSION);
		this.snakeYPosition.add(yPosition);
		this.snakeXPosition.add(xPosition - DEFAULT_ENTITY_DIMENSION * 2);
		this.snakeYPosition.add(yPosition);
		
		
		this.snakeXHead = snakeXPosition.get(0);
		this.snakeYHead = snakeYPosition.get(0);
		this.snakeSize = snakeXPosition.size();
	
		this.speed = DEFAULT_SPEED;
		this.pointsRate = 1;
		this.xMove = 0;
		this.yMove = 0;
	}
	
	/**
	 * Construtor que recebe outra snake como parâmetro
	 * para que seja feita a troca de snakes durante o jogo
	 * 
	 * @param game
	 * @param map
	 * @param oldSnake
	 */
	public Snake(Game game, Map map, Snake oldSnake) {
		super(game, map, oldSnake.getSnakeXHead(), oldSnake.getSnakeYHead());
		
		snakeXPosition = new ArrayList<Integer>();
		snakeYPosition = new ArrayList<Integer>();
		
		this.snakeXPosition = oldSnake.getSnakeXPosition();
		this.snakeYPosition = oldSnake.getSnakeYPosition();
		
		this.snakeXHead = oldSnake.getSnakeXPosition().get(0);
		this.snakeYHead = oldSnake.getSnakeYPosition().get(0);
		this.snakeSize = oldSnake.getSnakeSize();
		
		this.map = oldSnake.getMap();
	
		this.speed = oldSnake.getSpeed();
		this.pointsRate = 1;
		this.xMove = 0;
		this.yMove = 0;
	}
	
	/**
	 * Thread que troca a snake comum por uma snake especial por 8 segundos
	 * durante o game. Há 50% de chance de trocar para a KittySnake
	 * e 50% para trocar para a StarSnake. Após os 8 segundos, a thread
	 * é encerrada e a CommonSnake volta
	 */
	@Override 
	public synchronized void run() {
		thread = new Thread();
		thread.start();
		Random random = new Random();
		int snakeNumber = random.nextInt(2);
		if(snakeNumber == 0) {
			Snake.setCurrentSnake(new StarSnake(game, map, Snake.getCurrentSnake()));
		} else if(snakeNumber == 1) {
			Snake.setCurrentSnake(new KittySnake(game, map, Snake.getCurrentSnake()));
		}
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Snake.setCurrentSnake(new CommonSnake(game, map, Snake.getCurrentSnake()));
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Verifica colisão e input do usuário
	 */
	@Override
	public void update() {
		if(checkCollision()) {
			GameState.setGameOver(true);
			//State.setCurrentState(new GameOverState(game));
		}
		getInput();
	}
	
	/**
	 * Verifica o input do usuário no teclado
	 * 
	 * Obs: pode ocorrer problema quando a mudança de direção é muita rápida
	 * e a snake ainda não finalizou o movimento (andar o tamanho de uma parte dela)
	 */
	public void getInput() {
		if(game.getKeyManager().up) {
			this.yMove += -speed;
		} else if(game.getKeyManager().down) {
			this.yMove += speed;
		} else if(game.getKeyManager().left) {
			this.xMove += -speed;
		} else if(game.getKeyManager().right) {
			this.xMove += speed;
		}
		
		move();
		
	}
	
	/**
	 * Verifica o movimento nos eixos x e y
	 */
	public void move() {
		moveX();
		moveY();
	}
	
	/**
	 * Movimenta a snake no eixo x, atualizando a posição dela
	 */
	public void moveX() {
		if((int) xMove == DEFAULT_ENTITY_DIMENSION || (int) xMove == -DEFAULT_ENTITY_DIMENSION) {
			xPosition += (int) xMove;
			if(xPosition < 0) {
				xPosition = Display.WIDTH - DEFAULT_ENTITY_DIMENSION;
			} else if(xPosition > Display.WIDTH - DEFAULT_ENTITY_DIMENSION) {
				xPosition = 0;
			}
			
			// Atualiza a posição da snake após a movimentação
			snakeXPosition.add(0, xPosition);
			snakeYPosition.add(0, yPosition);
			snakeXPosition.remove(snakeSize);
			snakeYPosition.remove(snakeSize);
			
			this.snakeXHead = snakeXPosition.get(0);
			this.snakeYHead = snakeYPosition.get(0);
			
			xMove = 0;
		}
	}
	
	/**
	 * Movimenta a snake no eixo y, atualizando a posição dela
	 */
	public void moveY() {
		if((int) yMove == DEFAULT_ENTITY_DIMENSION || (int) yMove == -DEFAULT_ENTITY_DIMENSION) {
			yPosition += (int) yMove;
			if(yPosition < 0) {
				yPosition = Display.HEIGHT - DEFAULT_ENTITY_DIMENSION - Block.DEFAULT_BLOCK_DIMENSION;
			} else if(yPosition > Display.HEIGHT - DEFAULT_ENTITY_DIMENSION - Block.DEFAULT_BLOCK_DIMENSION) {
				yPosition = 0;
			}
			
			// Atualiza a posição da snake após a movimentação
			snakeXPosition.add(0, xPosition);
			snakeYPosition.add(0, yPosition);
			snakeXPosition.remove(snakeSize);
			snakeYPosition.remove(snakeSize);
			
			this.snakeXHead = snakeXPosition.get(0);
			this.snakeYHead = snakeYPosition.get(0);
			
			yMove = 0;
			
		}
	}
	
	/**
	 * Aumenta o tamanho da snake, atualizando os Arrays
	 */
	public void growSnake() {
		snakeXPosition.add(snakeXPosition.get(snakeSize - 1));
		snakeYPosition.add(snakeYPosition.get(snakeSize - 1));
		snakeSize++;
	}
	
	/**
	 * Volta a snake para o tamanho inicial (utilizada
	 * pela DecreaseFruit)
	 */
	public void decreaseSnake() {
		while(snakeXPosition.size() > 3) {
			snakeXPosition.remove(3);
			snakeYPosition.remove(3);
			snakeSize = snakeXPosition.size();
		}
	}
	
	/**
	 * Checa colisão com a própria snake e com os blocos
	 * 
	 * @return true
	 * 			Caso tenha acontencido colisão
	 * @return false
	 * 			Caso não tenha acontencido colisão
	 */
	public boolean checkCollision() {
		return collisionWithSnake() || collisionWithBlock();
	}
	
	/**
	 * Verifica colisão com a própria Snake
	 * 
	 * @return true
	 * 			Caso tenha acontencido colisão
	 * @return false
	 * 			Caso não tenha acontencido colisão
	 */
	public boolean collisionWithSnake() {
		for(int i = 1; i < this.snakeSize; i++) {
			if(this.snakeXPosition.get(i) == this.snakeXHead &&
					this.snakeYPosition.get(i) == this.snakeYHead) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Verifica colisão com os blocos sólidos
	 * 
	 * @return true
	 * 			Caso tenha acontencido colisão
	 * @return false
	 * 			Caso não tenha acontencido colisão
	 */
	public boolean collisionWithBlock() {
		return getMap().getBlock(snakeXHead / Block.DEFAULT_BLOCK_DIMENSION, snakeYHead / Block.DEFAULT_BLOCK_DIMENSION).isSolid();
	}
	
	// GETTERS E SETTERS
	
	public static Snake getCurrentSnake() {
		return currentSnake;
	}

	public static void setCurrentSnake(Snake currentSnake) {
		Snake.currentSnake = currentSnake;
	}

	public int getSnakeXHead() {
		return snakeXHead;
	}

	public int getSnakeYHead() {
		return snakeYHead;
	}

	public ArrayList<Integer> getSnakeXPosition() {
		return snakeXPosition;
	}

	public ArrayList<Integer> getSnakeYPosition() {
		return snakeYPosition;
	}

	public int getSnakeSize() {
		return snakeSize;
	}

	public float getSpeed() {
		return speed;
	}

	public int getPointsRate() {
		return pointsRate;
	}
	
}
