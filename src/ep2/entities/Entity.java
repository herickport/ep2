package ep2.entities;

import java.awt.Graphics;

import ep2.Game;
import ep2.maps.Map;

/**
 * Classe abstrata que será herdada pela Snake e Fruit 
 * 
 * @author herickport
 *
 */
public abstract class Entity {
	
	public static final int DEFAULT_ENTITY_DIMENSION = 24;
	
	protected Game game;
	protected Map map;
	protected int xPosition;
	protected int yPosition;
	
	public Entity(Game game, Map map, int xPosition, int yPosition) {
		this.game = game;
		this.map = map;
		this.xPosition = xPosition;
		this.yPosition = yPosition;
	}
	
	public abstract void update();
	
	public abstract void draw(Graphics g);

	// GETTERS E SETTERS
	
	public int getXPosition() {
		return xPosition;
	}

	public int getYPosition() {
		return yPosition;
	}

	public Game getGame() {
		return game;
	}

	public Map getMap() {
		return map;
	}
	
}
