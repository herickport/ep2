package ep2.entities.fruits;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.ImageIcon;

import ep2.Game;
import ep2.entities.snakes.Snake;
import ep2.maps.Map;

public class SimpleFruit extends Fruit {

	public SimpleFruit(Game game, Map map) {
		super(game, map);
		this.points = DEFAULT_POINTS;
	}
	
	/**
	 * Override da thread que seta a fruta na tela,
	 * na SimpleFruit não tem tempo de duração, ela
	 * fica na tela até ser 'comida' pela snake
	 */
	@Override
	public void run() {
		thread = new Thread();
		thread.start();
		setFruit();
	}
	
	/**
	 * Verifica se a snake comeu a fruta, em caso afirmativo
	 * a snake aumenta de tamanho e uma nova SimpleFruit é
	 * posicionada na tela do jogo
	 */
	@Override
	public void update() {
		if(eatFruit()) {
			Snake.getCurrentSnake().growSnake();
			setFruit();
		}
	}
	
	/**
	 * Desenha a SimpleFruit na tela do game
	 */
	public void draw(Graphics g) {
		ImageIcon image = new ImageIcon("./res/fruit.png");
		fruitImage = image.getImage();
		g.drawImage(fruitImage, xPosition, yPosition, null);
		g.setColor(Color.BLUE);
	}

}
