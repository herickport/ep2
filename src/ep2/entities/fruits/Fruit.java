package ep2.entities.fruits;

import java.awt.Image;
import java.util.Random;

import ep2.Game;
import ep2.blocks.Block;
import ep2.display.Display;
import ep2.entities.Entity;
import ep2.entities.snakes.Snake;
import ep2.maps.Map;

/**
 * Classe abstrata Fruit que será a base para todas as classe de fruta
 * 
 * @author herickport
 *
 */
public abstract class Fruit extends Entity implements Runnable {
	
	public static final int DEFAULT_POINTS = 6;
	
	Thread thread;
	
	protected Image fruitImage;
	protected int points;
	
	/**
	 * Construtor da classe Fruit
	 * 
	 * @param game
	 * @param map
	 */
	public Fruit(Game game, Map map) {
		super(game, map, -2 * DEFAULT_ENTITY_DIMENSION, -2 * DEFAULT_ENTITY_DIMENSION);
	}
	
	/**
	 * Seta a posição da fruta de forma aleatória e garante
	 * que a posição da fruta é uma posição válida, ou seja,
	 * não é em alguma parte da cobra ou em um bloco sólido
	 */
	public void setFruit() {
		Random random = new Random();
		do {
			this.yPosition = random.nextInt(Display.HEIGHT) % ((Display.HEIGHT - 48) / DEFAULT_ENTITY_DIMENSION) * DEFAULT_ENTITY_DIMENSION;
			this.xPosition = random.nextInt(Display.WIDTH) % (Display.WIDTH / DEFAULT_ENTITY_DIMENSION) * DEFAULT_ENTITY_DIMENSION;
		} while((Snake.getCurrentSnake().getSnakeXPosition().contains(getXPosition()) &&
				Snake.getCurrentSnake().getSnakeYPosition().contains(getYPosition())) || 
				(map.getBlock(xPosition / Block.DEFAULT_BLOCK_DIMENSION,
						yPosition / Block.DEFAULT_BLOCK_DIMENSION).isSolid()));
	}
	
	/**
	 * Thread que as 3 frutas especiais usam para ficar na
	 * tela do jogo por 5 segundos ou até ser 'comida' pela snake
	 */
	@Override
	public void run() {
		thread = new Thread();
		thread.start();
		setFruit();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		stop();
	}
	
	/**
	 * Método que encerra a thread
	 */
	public void stop() {
		try {
			thread.join();
			xPosition = -2 * DEFAULT_ENTITY_DIMENSION;
			yPosition = -2 * DEFAULT_ENTITY_DIMENSION;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Verifica se a fruta foi 'comida' pela snake
	 * 
	 * @return true
	 * 			Em caso positivo
	 * @return false
	 * 			Em caso negativo
	 */
	public boolean eatFruit() {
		if(Snake.getCurrentSnake().getSnakeXHead() == getXPosition() &&
			Snake.getCurrentSnake().getSnakeYHead() == getYPosition()) {
			return true;
		}
		
		return false;
	}
	
	// GETTERS E SETTERS
	
	public int getPoints() {
		return points;
	}
	
}