package ep2.entities.fruits;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import ep2.Game;
import ep2.maps.Map;
import ep2.states.GameState;

public class BombFruit extends Fruit {
	
	public BombFruit(Game game, Map map) {
		super(game, map);
	}
	
	/**
	 * Desenha a BombFruit na tela do game
	 */
	public void draw(Graphics g) {
		ImageIcon image = new ImageIcon("./res/bombFruit.gif");
		fruitImage = image.getImage();
		g.drawImage(fruitImage, xPosition, yPosition, null);
	}

	/**
	 * Caso a BombFruit seja comida pela snake
	 * vai para a tela de Game Over!
	 */
	@Override
	public void update() {
		if(eatFruit()) {
			stop();
			GameState.setGameOver(true);
			//State.setCurrentState(new GameOverState(game));
		}
	}

}
