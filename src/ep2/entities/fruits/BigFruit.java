package ep2.entities.fruits;

import java.awt.Graphics;
import java.util.Random;

import javax.swing.ImageIcon;

import ep2.Game;
import ep2.blocks.Block;
import ep2.display.Display;
import ep2.entities.snakes.Snake;
import ep2.maps.Map;

public class BigFruit extends Fruit {

	public BigFruit(Game game, Map map) {
		super(game, map);
		// A pontuação da BigFruit é dobrada
		this.points = DEFAULT_POINTS * 2;
	}
	
	/**
	 * caso a BigFruit seja 'comida' pela snake a thread é encerrada
	 */
	@Override
	public void update() {
		if(eatFruit()) {
			stop();
		}
	}

	/**
	 * Desenha a bigFruit na tela do game
	 */
	@Override
	public void draw(Graphics g) {
		ImageIcon image = new ImageIcon("./res/bigFruit.gif");
		fruitImage = image.getImage();
		g.drawImage(fruitImage, xPosition, yPosition, null);
	}
	
	/**
	 * Verifica se a snake comeu a BigFruit.
	 * É necessário override pelo fato da BigFruit ser maior
	 */
	@Override
	public boolean eatFruit() {
		if(Snake.getCurrentSnake().getSnakeXHead() == getXPosition()) {
			if(Snake.getCurrentSnake().getSnakeYHead() == getYPosition() ||
					Snake.getCurrentSnake().getSnakeYHead() == getYPosition() + DEFAULT_ENTITY_DIMENSION) {
				return true;
			}
			
			return false;
		} else if(Snake.getCurrentSnake().getSnakeXHead() == getXPosition() + DEFAULT_ENTITY_DIMENSION) {
			if(Snake.getCurrentSnake().getSnakeYHead() == getYPosition() ||
					Snake.getCurrentSnake().getSnakeYHead() == getYPosition() + DEFAULT_ENTITY_DIMENSION) {
				return true;
			}
			
			return false;
		}
		
		return false;
	}
	
	/**
	 * Seta a posição da fruta de forma aleatória e garante
	 * que a posição da fruta é uma posição válida, ou seja,
	 * não é em alguma parte da cobra ou em um bloco sólido.
	 * 
	 * É necessário override pelo fato da BigFruit ser maior
	 */
	@Override
	public void setFruit() {
		Random random = new Random();
		do {
			this.yPosition = random.nextInt(Display.HEIGHT) % ((Display.HEIGHT - 72) / DEFAULT_ENTITY_DIMENSION) * DEFAULT_ENTITY_DIMENSION;
			this.xPosition = random.nextInt(Display.WIDTH) % ((Display.WIDTH - DEFAULT_ENTITY_DIMENSION) / DEFAULT_ENTITY_DIMENSION) * DEFAULT_ENTITY_DIMENSION;
		} while((Snake.getCurrentSnake().getSnakeXPosition().contains(getXPosition()) &&
				Snake.getCurrentSnake().getSnakeYPosition().contains(getYPosition())) || 
				(map.getBlock(xPosition / Block.DEFAULT_BLOCK_DIMENSION,
						yPosition / Block.DEFAULT_BLOCK_DIMENSION).isSolid()) ||
				(map.getBlock((xPosition + DEFAULT_ENTITY_DIMENSION) / Block.DEFAULT_BLOCK_DIMENSION,
						yPosition / Block.DEFAULT_BLOCK_DIMENSION).isSolid()) ||
				(map.getBlock(xPosition / Block.DEFAULT_BLOCK_DIMENSION,
						(yPosition + DEFAULT_ENTITY_DIMENSION) / Block.DEFAULT_BLOCK_DIMENSION).isSolid()));
	}

}
