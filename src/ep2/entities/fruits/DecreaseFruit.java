package ep2.entities.fruits;

import java.awt.Graphics;

import javax.swing.ImageIcon;

import ep2.Game;
import ep2.entities.snakes.Snake;
import ep2.maps.Map;

public class DecreaseFruit extends Fruit {

	public DecreaseFruit(Game game, Map map) {
		super(game, map);
	}

	/**
	 * Ao comer a DecreaseFruit a snake volta pro tamanho inicial
	 */
	@Override
	public void update() {
		if(eatFruit()) {
			stop();
			Snake.getCurrentSnake().decreaseSnake();
		}
	}
	
	/**
	 * Desenha a DecreaseFruit na tela do game
	 */
	@Override
	public void draw(Graphics g) {
		ImageIcon image = new ImageIcon("./res/decreaseFruit.gif");
		fruitImage = image.getImage();
		g.drawImage(fruitImage, xPosition, yPosition, null);
	}

}
