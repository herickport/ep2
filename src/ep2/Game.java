package ep2;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import ep2.display.Display;
import ep2.input.KeyManager;
import ep2.states.MenuState;
import ep2.states.State;

/**
 * Classe que faz todo o jogo rodar, iniciando o display,
 * as threads, atualização e renderização do game, etc
 * 
 * @author herickport
 *
 */
public class Game implements Runnable {

	private Display display;
	
	private BufferStrategy bs;
	private Graphics g;
	
	private Thread thread;
	private boolean running;
	
	private KeyManager keyManager;
	
	/**
	 * Construtor da classe
	 */
	public Game() {
		keyManager = new KeyManager();
	}
	
	/**
	 * Cria um display e seta o menuState como o State
	 * a ser exibido no display inicialmente
	 */
	private void init() {
		display = new Display();
		display.getFrame().addKeyListener(keyManager);
		
		State.setCurrentState(new MenuState(this));
	}
	
	/**
	 * Update do jogo (600 fps)
	 */
	private void update() {
		keyManager.update();
		
		if(State.getCurrentState() != null) {
			State.getCurrentState().update();
		}
	}
	
	/**
	 * Renderização do jogo (600 fps)
	 */
	private void draw() {
		bs = display.getCanvas().getBufferStrategy();
		if(bs == null) {
			display.getCanvas().createBufferStrategy(3);
			return;
		}
		
		g = bs.getDrawGraphics();
		g.clearRect(0, 0, Display.WIDTH, Display.HEIGHT);
		
		if(State.getCurrentState() != null) {
			State.getCurrentState().draw(g);
		}
		
		bs.show();
		g.dispose();
	}
	
	/**
	 * Inicia a thread principal do game
	 */
	public synchronized void start() {
		if(running) {
			return;
		}
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	/**
	 * Encerra a thread
	 */
	public synchronized void stop() {
		if(!running) {
			return;
		}
		running = false;
		
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Game Loop, responsável por atualizar e desenhar
	 * o jogo a 600 fps enquanto este estiver em execução
	 */
	@Override
	public void run() {
		// inicia
		init();
		
		// Calculo do fps
		int fps = 600;
		double timePerTick = 1000000000 / fps;
		double delta = 0;
		long now;
		long lastTime = System.nanoTime();
		
		while(running) {
			now = System.nanoTime();
			delta += (now - lastTime) / timePerTick;
			lastTime = now;
			
			if(delta >= 1) {
				// O jogo é atualizado 600 vezes por segundo
				update();
				draw();
				delta--;
			}
		}
		
		stop();
	}
	
	// GETTERS E SETTERS
	
	public KeyManager getKeyManager() {
		return keyManager;
	}

	public Display getDisplay() {
		return display;
	}
	
}
