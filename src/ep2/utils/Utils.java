package ep2.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Classe que auxilia carregar os mapas de um arquivo .txt
 */
public class Utils {

	public static String loadFileAsString(String path) {
		StringBuilder builder = new StringBuilder();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line;
			while((line = br.readLine()) != null) {
				builder.append(line + "\n");
			}
			
			br.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return builder.toString();
	}
	
	public static int parseInt(String number) {
		try {
			return Integer.parseInt(number);
		} catch(NumberFormatException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * Salva a pontuação obtida em um arquivo de texto
	 * 
	 * @param path
	 * @param points
	 */
	public static void loadFileAndWritePoints(String path, int points) {	
		JFrame frame = new JFrame("");
		ArrayList<String> linesArq = new ArrayList<String>();		

	    // Pop-Up para o usuário digitar o nome que será salvo no ranking
	    String name = JOptionPane.showInputDialog(frame, "Você conquistou " + points + " pontos!\n"
	    		+ "Digite seu nome para salvar a pontuação no arquivo:");
	
        BufferedWriter buffWrite;
        Integer totalPoints = points;
        
        // Lê todo o arquivo e guarda em um array para sobrescrever depois
	    try {
	        FileReader arq = new FileReader(path);
	        BufferedReader buffReader = new BufferedReader(arq);
	   
	        // Lê a primeira linha do arquivo
	        String linha = buffReader.readLine();
	        
	        // Lê todas as linhas do arquivo e salva no array
	        while (linha != null) {
	        	linesArq.add(linha);
	        	linha = buffReader.readLine();
	        }
	   
	        arq.close();
	    } catch (IOException e) {
	          	System.err.printf("Erro na abertura do arquivo: %s.\n",
	            e.getMessage());
	    }
       
	    // Escreve no arquivo
		try {
			buffWrite = new BufferedWriter(new FileWriter(path));
			
			// Percorre o array e escreve no arquivo
			for(int i = 0; i < linesArq.size(); i++) {
				buffWrite.append(linesArq.get(i) + "\n");
			}
			
			// Escreve a pontuação do último jogador
			buffWrite.append(name + " " + totalPoints + "\n");
			linesArq.add(totalPoints.toString());
			buffWrite.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
    }
}
