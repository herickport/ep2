package ep2.blocks;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;

public class Block {
	
	public static final int DEFAULT_BLOCK_DIMENSION = 48;
	
	private Image blockImage;
	
	/**
	 * Construtor padrão dos blocos do mapa
	 * 
	 * @param image
	 */
	public Block(ImageIcon image) {
		this.blockImage = image.getImage();
	}
	
	/**
	 * Desenha o bloco na tela no pixel x, y
	 * 
	 * @param g
	 * @param x
	 * @param y
	 */
	public void draw(Graphics g, int x, int y) {
		g.drawImage(blockImage, x, y, null);
	}
	
	public boolean isSolid() {
		return false;
	}

}
