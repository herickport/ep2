package ep2.blocks;

import javax.swing.ImageIcon;

public class BackgroundBlock extends Block {

	public BackgroundBlock() {
		super(new ImageIcon("./res/background.png"));
	}

}
