package ep2.blocks;

import javax.swing.ImageIcon;

public class SolidBlock extends Block {
	
	public SolidBlock() {
		super(new ImageIcon("./res/block.png"));
	}
	
	/**
	 * Bloco sólido no mapa
	 */
	@Override
	public boolean isSolid() {
		return true;
	}

}
