package ep2.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyManager implements KeyListener {

	private boolean[] keys;
	public boolean up, down, left, right, space;
	public boolean enter, map0, map1, map2, map3, map4, map5;
	
	/**
	 * Construtor
	 */
	public KeyManager() {
		keys = new boolean[256];
	}
	
	/**
	 * Seta o valor lógico dos atributos de acordo com a tecla clicada
	 */
	public void update() {
		up = keys[KeyEvent.VK_UP] || keys[KeyEvent.VK_W];
		down = keys[KeyEvent.VK_DOWN] || keys[KeyEvent.VK_S];
		left = keys[KeyEvent.VK_LEFT] || keys[KeyEvent.VK_A];
		right = keys[KeyEvent.VK_RIGHT] || keys[KeyEvent.VK_D];
		space = keys[KeyEvent.VK_SPACE];
		enter = keys[KeyEvent.VK_ENTER];
		map0 = keys[KeyEvent.VK_0] || keys[KeyEvent.VK_NUMPAD0];
		map1 = keys[KeyEvent.VK_1] || keys[KeyEvent.VK_NUMPAD1];
		map2 = keys[KeyEvent.VK_2] || keys[KeyEvent.VK_NUMPAD2];
		map3 = keys[KeyEvent.VK_3] || keys[KeyEvent.VK_NUMPAD3];
		map4 = keys[KeyEvent.VK_4] || keys[KeyEvent.VK_NUMPAD4];
		map5 = keys[KeyEvent.VK_5] || keys[KeyEvent.VK_NUMPAD5];
	}
	
	/**
	 * Verifica as teclas clicadas pelo usuário e atualiza o vetor
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		if((e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_W) && down == false) {
			keys[KeyEvent.VK_UP] = true;
			keys[KeyEvent.VK_LEFT] = false;
			keys[KeyEvent.VK_RIGHT] = false;
			keys[KeyEvent.VK_W] = true;
			keys[KeyEvent.VK_A] = false;
			keys[KeyEvent.VK_D] = false;
		}
		else if((e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_S) && up == false) {
			keys[KeyEvent.VK_DOWN] = true;
			keys[KeyEvent.VK_LEFT] = false;
			keys[KeyEvent.VK_RIGHT] = false;
			keys[KeyEvent.VK_S] = true;
			keys[KeyEvent.VK_A] = false;
			keys[KeyEvent.VK_D] = false;
		}
		else if((e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_A) && right == false) {
			keys[KeyEvent.VK_LEFT] = true;
			keys[KeyEvent.VK_UP] = false;
			keys[KeyEvent.VK_DOWN] = false;
			keys[KeyEvent.VK_A] = true;
			keys[KeyEvent.VK_W] = false;
			keys[KeyEvent.VK_S] = false;
		}
		else if((e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == KeyEvent.VK_D) && left == false) {
			keys[KeyEvent.VK_RIGHT] = true;
			keys[KeyEvent.VK_UP] = false;
			keys[KeyEvent.VK_DOWN] = false;
			keys[KeyEvent.VK_D] = true;
			keys[KeyEvent.VK_W] = false;
			keys[KeyEvent.VK_S] = false;
		} else if(e.getKeyCode() == KeyEvent.VK_SPACE) {
			keys[KeyEvent.VK_SPACE] = true;
			keys[KeyEvent.VK_RIGHT] = false;
			keys[KeyEvent.VK_UP] = false;
			keys[KeyEvent.VK_DOWN] = false;
			keys[KeyEvent.VK_LEFT] = false;
			keys[KeyEvent.VK_D] = false;
			keys[KeyEvent.VK_W] = false;
			keys[KeyEvent.VK_S] = false;
			keys[KeyEvent.VK_A] = false;
		} else if(e.getKeyCode() == KeyEvent.VK_0 || e.getKeyCode() == KeyEvent.VK_NUMPAD0) {
			keys[KeyEvent.VK_0] = true;
			keys[KeyEvent.VK_NUMPAD0] = true;
			keys[KeyEvent.VK_RIGHT] = true;
			keys[KeyEvent.VK_D] = true;
		} else if(e.getKeyCode() == KeyEvent.VK_1 || e.getKeyCode() == KeyEvent.VK_NUMPAD1) {
			keys[KeyEvent.VK_1] = true;
			keys[KeyEvent.VK_NUMPAD1] = true;
			keys[KeyEvent.VK_RIGHT] = true;
			keys[KeyEvent.VK_D] = true;
		} else if(e.getKeyCode() == KeyEvent.VK_2|| e.getKeyCode() == KeyEvent.VK_NUMPAD2) {
			keys[KeyEvent.VK_2] = true;
			keys[KeyEvent.VK_NUMPAD2] = true;
			keys[KeyEvent.VK_RIGHT] = true;
			keys[KeyEvent.VK_D] = true;
		} else if(e.getKeyCode() == KeyEvent.VK_3 || e.getKeyCode() == KeyEvent.VK_NUMPAD3) {
			keys[KeyEvent.VK_3] = true;
			keys[KeyEvent.VK_NUMPAD3] = true;
			keys[KeyEvent.VK_RIGHT] = true;
			keys[KeyEvent.VK_D] = true;
		} else if(e.getKeyCode() == KeyEvent.VK_4 || e.getKeyCode() == KeyEvent.VK_NUMPAD4) {
			keys[KeyEvent.VK_4] = true;
			keys[KeyEvent.VK_NUMPAD4] = true;
			keys[KeyEvent.VK_RIGHT] = true;
			keys[KeyEvent.VK_D] = true;
		} else if(e.getKeyCode() == KeyEvent.VK_5 || e.getKeyCode() == KeyEvent.VK_NUMPAD5) {
			keys[KeyEvent.VK_5] = true;
			keys[KeyEvent.VK_NUMPAD5] = true;
			keys[KeyEvent.VK_RIGHT] = true;
			keys[KeyEvent.VK_D] = true;
		} else if(e.getKeyCode() == KeyEvent.VK_ENTER) {
			keys[KeyEvent.VK_ENTER] = true;
			keys[KeyEvent.VK_RIGHT] = true;
			keys[KeyEvent.VK_D] = true; 
		}
	}
	
	/**
	 * Atualiza o vetor quando o usuário deixa de apertar a tecla
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_SPACE) {
			keys[KeyEvent.VK_SPACE] = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_0 || e.getKeyCode() == KeyEvent.VK_NUMPAD0) {
			keys[KeyEvent.VK_0] = false;
			keys[KeyEvent.VK_NUMPAD0] = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_1 || e.getKeyCode() == KeyEvent.VK_NUMPAD1) {
			keys[KeyEvent.VK_1] = false;
			keys[KeyEvent.VK_NUMPAD1] = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_2|| e.getKeyCode() == KeyEvent.VK_NUMPAD2) {
			keys[KeyEvent.VK_2] = false;
			keys[KeyEvent.VK_NUMPAD2] = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_3 || e.getKeyCode() == KeyEvent.VK_NUMPAD3) {
			keys[KeyEvent.VK_3] = false;
			keys[KeyEvent.VK_NUMPAD3] = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_4 || e.getKeyCode() == KeyEvent.VK_NUMPAD4) {
			keys[KeyEvent.VK_4] = false;
			keys[KeyEvent.VK_NUMPAD4] = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_5 || e.getKeyCode() == KeyEvent.VK_NUMPAD5) {
			keys[KeyEvent.VK_5] = false;
			keys[KeyEvent.VK_NUMPAD5] = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER) {
			keys[KeyEvent.VK_ENTER] = false;
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

}
