# EP2 - OO 2019.2 (UnB - Gama)

Hérick Ferreira de Souza Portugues - 18/0033034

# Especificações e Requisitos

O projeto foi contruído utilizando a IDE Eclipse e o Java 11

## Descrição

O projeto é um jogo Snake que inicia com um menu (bem fraco esteticamente) que permite ao usuário escolher o mapa em que deseja jogar (apertando um número entre 0 e 5) ou jogar um mapa aleatório (clicando enter), após escolher o mapa o usuário é direcionado à tela do jogo, onde pode controlar a snake tanto com "WASD" quanto com as setinhas do teclado (up, down, left, right), além disso a partida pode ser 'pausada' clicando na tecla "espaço". Após iniciar o jogo toda a dinâmica do game ocorre, que pode ser conferida com mais detalhes abaixo, com a explicação das threads das snakes e frutas e a imagem dos mapas.

## Game Loop

O jogo roda um game loop acionado por um thread que troca entre States(menu, game, gameOver) de acordo com as entradas do usuário no teclado e que é atualizado a 600 fps, fazendo todos os updates e renderizando a tela.

## Tipos de Snakes

* **Comum:** A Snake classica (Branca), consegue atravessar as bordas da tela, mas não possui habilidades especiais.
* **Kitty:** Essa Snake tem a habilidade de atravessar as barreiras do jogo, mas não pode atravessar as bordas nem a si mesma. ![kittySnake](./res/kittySnake.gif)
* **Star:** Recebe o dobro de pontos ao comer as frutas e também pode atravessar as bordas da tela. ![starSnake](./res/starSnake.gif)

**OBS.:** O jogo é iniciado com a snake comum e a cada 60 pontos é acionada uma thread que troca aleatoriamente a snake para a kitty ou star (50% de chance para cada) e permanece por 8 segundos até a thread ser encerrada e voltar para a snake comum.

## Colisões

As Snakes colidem com barreiras no interior do jogo, salvo a Snake Kitty que atravessa barreiras. Com exceção da Snake Kitty, todas as snakes podem atravessar as bordas do jogo. Quando há algum tipo de colisão (nas barreiras ou em si mesma), a cobra morre, abre uma janela para que o usuário possa escrever o nome que será salvo no arquivo junto com sua pontuação e o game é mudado para a tela de Game Over!

## Frutas

As frutas são elementos que aparecem aleatoriamente e são os objetivos das Snakes.

* **Simple Fruit:** Fruta comum, dá um ponto e aumenta o tamanho da cobra. ![simpleFruit](./res/fruit.png)
* **Bomb Fruit:** Essa fruta deve levar a morte da Snake. ![bombFruit](./res/bombFruit.gif)
* **Big Fruit:** Dá o dobro de pontos da Simple Fruit e aumenta o tamanho da cobra da mesma forma que a Simple Fruit. ![bigFruit](./res/bigFruit.gif)
* **Decrease Fruit:** Diminui o tamanho da cobra para o tamanho inicial, sem fornecer nem retirar pontos. ![decreaseFruit](./res/decreaseFruit.gif)

**OBS.:** As frutas são geradas por thread, de modo que a Simple Fruit aparece durante todo o jogo, sendo trocada de lugar sempre que a snake a coleta, enquanto as demais frutas aparecem a cada 30 pontos aleatoriamente (50% de chance de aparecer a Big Fruit, 25% de aparecer a Bomb Fruit e 25% de aparecer a Decrease Fruit) e permanecem na tela durante 5 segundos ou até que sejam coletadas pela snake.

## Pontos

Os pontos são calculados de acordo com as frutas coletadas (6 pontos para cada fruta coletada, exceto a Big Fruit que vale 12) e com a snake que coletou (Star Snake tem pontuação dobrada).

## Mapas

O jogo possui 6 mapas, que foram baseados nos mapas do jogo Snake Xenzia (snake dos celulares nokia) e são carregados de um arquivo .txt, sendo eles:

* **Map 0:** ![map0](./res/maps/map0.png)

* **Map 1:** ![map1](./res/maps/map1.png)

* **Map 2:** ![map2](./res/maps/map2.png)

* **Map 3:** ![map3](./res/maps/map3.png)

* **Map 4:** ![map4](./res/maps/map4.png)

* **Map 5:** ![map5](./res/maps/map5.png)
